<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $item = new Item([
          'name' => $request->get('userpost'),
          'cid' => $request->get('cid'),
          'tel' => $request->get('tel'),
          'email' => $request->get('email'),
          'detail' => $request->get('detail'),
          'status' => $request->get('status'),
          'admin' => $request->get('admin')
        ]);
        $item->save();
        return response()->json('Successfully added');
    }
}
